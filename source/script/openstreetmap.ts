// NOTE: using overpass-ts causes CORS errors, so we only use their typings for now.
import { overpassJson } from "overpass-ts";
import type { OverpassJson } from "overpass-ts";

// Choose a server to query for openstreetmap data.
// NOTE: Mail.ru, 2 servers, 56 cores - free and no rate limits, no questions asked.
const overpassInterpreter = 'https://maps.mail.ru/osm/tools/overpass/api/interpreter?data=';

// NOTE: Kumi systems, 4 servers, 20 cores - free and no rate limits, but large scale projects should talk with them first.
//const overpassInterpreter = 'https://overpass.kumi.systems/api/interpreter?data=';

// NOTE: Default overpass service, 2 servers, 4 cores. Free, but rate limited and sometimes problematic.
//const overpassInterpreter = 'https://overpass-api.de/api/interpreter?data=';
//const overpassInterpreter = 'https://api.openstreetmap.fr/api/interpreter?data=';

/**
 * Queries the default overpass server and returns the parsed JSON data.
 *
 * @param query   an overpass API query.
 *
 * @returns the query results.
 */
export const queryOverpass = async function(query: string): Promise<OverpassJson>
{
	try
	{
		// Send the query to the overpass interpreting server.
		const queryResponse = await fetch(overpassInterpreter + query);

		// Parse the response.
		const queryResult = await queryResponse.json();

		// Return the parsed data.
		return queryResult as OverpassJson;
	}
	catch(error)
	{
		// Log the error.
		console.error(`Could not query overpass server (${overpassInterpreter}).`);
		console.error(query, error);

		// Re-throw the error to be handled upstreams.
		throw(error);
	}
}

/**
 * Queries the default overpass server for a list of regions related to a given coordinate.
 *
 * @param latitude    the latitude of the coordinate, in the range of -90 ~ +90.
 * @param longitude   the longitude of the coordinate, in the range of -180 ~ +180.
 *
 * @returns an overpass result with the areas.
 */
export const getAreasFromPosition = async function(latitude: number, longitude: number): Promise<OverpassJson>
{
	// Throw an error if the provided latitude is outside of the valid range.
	if((latitude < -90) || (latitude > 90))
	{
		throw(new RangeError(`Provided latitude () is outside of the accepted range of -90 ~ +90`));
	}

	// Throw an error if the provided longitude is outside of the valid range.
	if((longitude < -180) || (longitude > 180))
	{
		throw(new RangeError(`Provided longitude () is outside of the accepted range of -180 ~ +180`));
	}

	// Build a search query for areas around a given coordinate.
	const query = `[out:json];is_in(${latitude},${longitude});out;`;

	// Query the overpass server for the areas.
	const result = await queryOverpass(query);

	// Return the areas.
	return result;
}

/**
 * Queries the default overpass server for a list of relevant points in a given area.
 *
 * @param areaId      the Id number of the area to get points in.
 * @param timestamp   a timestamp used to filter out old entries.
 *
 * @returns an overpass result with the points.
 */
export const getPointsFromArea = async function(areaId: number, timestamp: string): Promise<OverpassJson>
{
	// set up filter on area and limiter to entries that have been updated recently.
	const filterOnArea    = `area(${areaId})->.a`;
	const limitToChanges = `node._(newer:"${timestamp}")`;

	// Prepare filters on crypto related data.
	const hasPaymentBitcoin      = 'node["payment:bitcoin"](area.a)'
	const hasPaymentBitcoinCash  = 'node["payment:bitcoincash"](area.a)';
	const hasCurrencyBitcoinCash = 'node["currency:BCH"](area.a)';

	// Set up a filter for match either bitcoin or bitcoin cash accepted.
	const filterOnCrypto = `(${hasPaymentBitcoin};${hasPaymentBitcoinCash};${hasCurrencyBitcoinCash};)`;

	// Assemble the final query.
	const query = `[out:json];${filterOnArea};${filterOnCrypto};${limitToChanges};out;`;

	// Query the overpass server for the points.
	const result = await queryOverpass(query);

	// Return the areas.
	return result;
}
